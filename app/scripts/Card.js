import * as wanakana from 'wanakana'

export class Card {
  constructor (id) {
    this._id = id
    this._card = document.querySelector(id)
    this._input = document.querySelector(id + ' input')
    this._image = document.querySelector(id + ' img')
    this._counter = document.querySelector(id + ' .counter')
    this._onSubmit = () => {}

    this._input.onkeyup = (event) => {
      if (event.key === 'Enter') {
        let answer = this.answer
        let n = answer.length
        if (answer[n - 1] === 'n') {
          answer = answer.slice(0, n - 1) + 'ん'
          this.answer = answer
        }

        this._onSubmit(answer)
        this.answer = ''
      }
    }

    wanakana.bind(this._input)
  }

  get answer () {
    return this._input.value
  }

  get counter () {
    return +this._counter.innerText
  }

  set counter (value) {
    this._counter.innerText = value
  }

  set answer (value) {
    this._input.value = value
  }

  set image (value) {
    this._image.src = value
  }

  set onSubmit (value) {
    this._onSubmit = value
  }
}
