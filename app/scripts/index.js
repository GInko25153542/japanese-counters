import './../styles/reset.scss'
import './../styles/main.scss'
import './../styles/card.scss'
import './counters.json'
import './../images/apple.svg'
import './../images/bottle.svg'
import './../images/pencil.svg'

import { Game } from './Game'

if (process.env.NODE_ENV !== 'production') {
  require('./../index.pug')
}
window.onload = () => {
  let game = new Game()
  if (game.hasOwnProperty('foo')) {
    console.log('bar')
  }
}
