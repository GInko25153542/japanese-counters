export class JapaneseCounter {
  // Init ----------------------------------
  constructor (kanji, images, readings) {
    this._kanji = kanji
    this._images = images
    this._readings = readings

    this._unusedReadings = this._readings.map((el, i) => [(i + 1), el]).sort(() => Math.random() - 1)
    this._usedReadings = []
    this._currentReadingInUse = -1
  }
  // Public ----------------------------------
  /**
   * Check if user's answer is correct
   * @param answer user's string hiragana answer
   * @returns {boolean} true if it's correct
   */
  checkAnswer (answer) {
    let result = this._unusedReadings[this._currentReadingInUse][1] === answer
    if (result) {
      let [reading] = this._unusedReadings.splice(this._currentReadingInUse, 1)
      this._usedReadings.push(reading)
    }
    return result
  }
  // Getters and Setters ----------------------------------
  /**
   * Gives a number and picture so user can write correct japanese counter
   * @returns {[number, string]} first el is counter, second el is image url
   */
  getQuestion () {
    let id = Math.floor(Math.random() * this._images.length)
    let image = this._images[id]

    id = Math.floor(Math.random() * this._unusedReadings.length)
    this._currentReadingInUse = id
    let counter = this._unusedReadings[id][0]

    return [image, counter]
  }

  /**
   * Checks if there is no more unanswered readings
   * @returns {boolean} true if no readings left
   */
  get isCompleate () {
    return this._usedReadings.length === this._readings.length
  }

  /**
   * Returns amount of readings in progress
   * @returns {*}
   */
  get readingsLeft () {
    return this._unusedReadings.length
  }
}
