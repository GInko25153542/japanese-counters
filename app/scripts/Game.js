import { Card } from './Card'
import { JapaneseCounter } from './JapaneseCounter'

export class Game {
  constructor () {
    this._card = new Card('#card')
    this._card.onSubmit = this.checkAnswer.bind(this)
    this._counters = []
    this._currentCounterID = -1

    fetch('/scripts/counters.json')
      .then(response => response.json())
      .then(data => {
        for (let key in data) {
          let hiragana = data[key].hiragana
          let images = data[key].images

          let counter = new JapaneseCounter(key, images, hiragana)
          this._counters.push(counter)
        }
        this.setNewQuestion()
      })
      .catch(reason => alert(reason))
  }

  setNewQuestion () {
    if (this._counters.length > 0) {
      this._currentCounterID = Math.floor(Math.random() * this._counters.length)
      let [image, counter] = this._counters[this._currentCounterID].getQuestion()
      this._card.image = image
      this._card.counter = counter
    }
  }

  checkAnswer (value) {
    let counter = this._counters[this._currentCounterID]
    if (counter.checkAnswer(value)) {
      this.goodAnswer()
      if (counter.isFinished) {
        this._counters.remove(counter)
      }
    } else {
      this.badAnswer()
    }
  }

  goodAnswer () {
    alert('CORRECT!')
    this.setNewQuestion()
  }

  badAnswer () {
    alert('Bad answer')
    this.setNewQuestion()
  }
}
